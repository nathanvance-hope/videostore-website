var app = angular.module ("imdb");

app.controller('MovieSearchCtrl', function($scope, Restangular) {
	$scope.years = [];
    for ($i = 1940; $i <= 2010; $i++) {
    	$scope.years.push($i);
    }
    
    $scope.Year = -1;
    $scope.MovieTitle = "";
    $scope.Director = "";
    $scope.RunningTime = -1;
    $scope.runningTimeComparator = "<=";
    
    $scope.movies = [];
    $scope.searchPerformed = false;
     
    $scope.searchDisabled = function () {
    	return $scope.MovieTitle.length == 0 && $scope.Director.length == 0 && $scope.Year == -1 && $scope.Director.length == 0 && (!$scope.RunningTime || $scope.RunningTime.length == 0);
    }
    
    $scope.searchForMovies = function () {
    	var MovieSearch = Restangular.all("/movie/search");
    	
    	var searchParameters = {
    		MovieTitle: "",
    		PrimaryGenre: "",
    		Year: $scope.Year,
    		Director: "",
    		runningTimeComparator: $scope.runningTimeComparator,
    		runningTime: -1
    	};
    	
    	if ($scope.MovieTitle.length > 0) {
    		searchParameters.MovieTitle = $scope.MovieTitle;
    	}
    	
    	if ($scope.PrimaryGenre && $scope.PrimaryGenre != "null") {
    		searchParameters.PrimaryGenre = $scope.PrimaryGenre;
    	}
    	
    	if ($scope.Director.length > 0) {
    		searchParameters.Director = $scope.Director;
    	}
    	
    	if ($scope.RunningTime && $scope.RunningTime.length > 0) {
    		searchParameters.RunningTime = $scope.RunningTime;
    	}
    	
    	MovieSearch.post(searchParameters).then(
    			function (movies) {
    				$scope.searchPerformed = true;
    				$scope.movies = movies;
    			}
    		);
    };

    $scope.makeGenreList = function(movie) {
        var names = _.map(movie.OtherGenres, function(genre) {
            return genre.Name;
        });
        return names.join(", ");
    };
    
    Restangular.all("genre").getList().then(
    	function (genres) {
    		$scope.genres = genres;
    	}
    );
    
});