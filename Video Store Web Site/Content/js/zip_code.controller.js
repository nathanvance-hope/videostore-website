var app = angular.module ("imdb");

app.controller('ZipCodeCtrl', function($scope, Restangular) {

    $scope.newCode = {
        "Code": "",
        "City": "",
        "State": ""
    };

    $scope.sortOrder = 'City';

    Restangular.all("zipcode").getList().then(
        function(zipcodes) {
            $scope.zipcodes = zipcodes;
        }
    );

    $scope.saveNewZipCode = function(newCode) {
        Restangular.all("zipcode").post(newCode).then(
            function(zipcode) {
                $scope.zipcodes.push(zipcode);
                $scope.newCode.Code = "";
                $scope.newCode.City = "";
                $scope.newCode.State = "";
            }
        );
    }
});