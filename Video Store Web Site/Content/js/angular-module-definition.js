var app = angular.module ("imdb", ['restangular']);

app.config(function(RestangularProvider) {
	RestangularProvider.setBaseUrl('/api');
});