var app = angular.module ("imdb");

app.controller('ActorSearchCtrl', function($scope, Restangular) {
	$scope.states = {
	     "AL": "Alabama",
	     "AK": "Alaska",
	     "AS": "American Samoa",
	     "AZ": "Arizona",
	     "AR": "Arkansas",
	     "CA": "California",
	     "CO": "Colorado",
	     "CT": "Connecticut",
	     "DE": "Delaware",
	     "DC": "District Of Columbia",
	     "FM": "Federated States Of Micronesia",
	     "FL": "Florida",
	     "GA": "Georgia",
	     "GU": "Guam",
	     "HI": "Hawaii",
	     "ID": "Idaho",
	     "IL": "Illinois",
	     "IN": "Indiana",
	     "IA": "Iowa",
	     "KS": "Kansas",
	     "KY": "Kentucky",
	     "LA": "Louisiana",
	     "ME": "Maine",
	     "MH": "Marshall Islands",
	     "MD": "Maryland",
	     "MA": "Massachusetts",
	     "MI": "Michigan",
	     "MN": "Minnesota",
	     "MS": "Mississippi",
	     "MO": "Missouri",
	     "MT": "Montana",
	     "NE": "Nebraska",
	     "NV": "Nevada",
	     "NH": "New Hampshire",
	     "NJ": "New Jersey",
	     "NM": "New Mexico",
	     "NY": "New York",
	     "NC": "North Carolina",
	     "ND": "North Dakota",
	     "MP": "Northern Mariana Islands",
	     "OH": "Ohio",
	     "OK": "Oklahoma",
	     "OR": "Oregon",
	     "PW": "Palau",
	     "PA": "Pennsylvania",
	     "PR": "Puerto Rico",
	     "RI": "Rhode Island",
	     "SC": "South Carolina",
	     "SD": "South Dakota",
	     "TN": "Tennessee",
	     "TX": "Texas",
	     "UT": "Utah",
	     "VT": "Vermont",
	     "VI": "Virgin Islands",
	     "VA": "Virginia",
	     "WA": "Washington",
	     "WV": "West Virginia",
	     "WI": "Wisconsin",
	     "WY": "Wyoming"
	};
    
	$scope.stateAbbreviations = Object.keys ($scope.states);
	$scope.stateNames = []
	_.each(Object.keys($scope.states), function(value) {$scope.stateNames.push($scope.states[value]);});
	
    $scope.stageFirstName = "";
    $scope.stageLastName = "";
    $scope.birthCity = "";
    $scope.birthStateOrProvince = "null";
    
    $scope.actors = [];
    $scope.searchPerformed = false;
    
    $scope.selectedActor = undefined;
    
    $scope.showMoviesForActor = function (actor) {
    	$scope.selectedActor = actor;
    	var RoleSearch = Restangular.one("Actor", actor.actorID).getList("Roles").then
    	(function (roles) {
    		$scope.selectedActor.roles = roles;
    	});
    };
    
    $scope.searchDisabled = function () {
    	return $scope.stageFirstName.length == 0 && $scope.stageLastName.length == 0 && 
    	       $scope.birthCity.length == 0 && $scope.birthStateOrProvince == "null";
    }
    
    $scope.searchForActors = function () {
    	var ActorSearch = Restangular.all("actor/search");
    	
    	var searchParameters = {
    		stageFirstName: "",
    		stageLastName: "",
    		birthCity: "",
    		birthStateOrProvince: ""
    	};
    	
    	if ($scope.stageFirstName.length > 0) {
    		searchParameters.stageFirstName = $scope.stageFirstName;
    	}
    	
       	if ($scope.stageLastName.length > 0) {
    		searchParameters.stageLastName = $scope.stageLastName;
    	}
    	
       	if ($scope.birthCity.length > 0) {
    		searchParameters.birthCity = $scope.birthCity;
    	}
       	
       	if ($scope.birthStateOrProvince.length > 0 && $scope.birthStateOrProvince != "null") {
    		searchParameters.birthStateOrProvince = $scope.birthStateOrProvince;
    	}
       	
    	ActorSearch.post(searchParameters).then(
    			function (actors) {
    				$scope.searchPerformed = true;
    				$scope.actors = actors;
    			}
    		);
    };
});