﻿using System.Collections.Generic;
using Mappings;
using Nancy;
using Nancy.ModelBinding;

namespace Video_Store_Web_Site.API
{
    public class ZipCode : BaseModule
    {
        public ZipCode() : base("zipcode")
        {
            var dictionary = new Dictionary<string, Model.ZipCode>();
            dictionary[Holland.Code] = Holland;
            dictionary[Zeeland.Code] = Zeeland;

            Get["/"] = _ =>
            {
                var session = _sessionFactory.GetCurrentSession();
                return session.QueryOver<Model.ZipCode>().List<Model.ZipCode>();
            };

            Get["/{code}"] = parameters =>
            {
                //  Replace this with code that
                //  1.  Gets the current session
                //  2.  Begins a transaction
                //  3.  Uses session.CreateQuery to create a query on the ZipCode object, looking up the ZipCode whose Code property matches
                //      the parameter passed as code 
                //  4.  Stores the query result in a variable, using the UniqueResult() method on the Query object
                //  5.  Commits the transaction
                //  6.  Returns the ZipCode object that was located in step 4
                return dictionary[parameters.code];
            };

            Post["/"] = _ =>
            {
                var saveParameters = this.Bind<Model.ZipCode>();
                var session = _sessionFactory.GetCurrentSession();
                using (var t = session.BeginTransaction())
                {
                    session.Save(saveParameters);
                    t.Commit();
                    return saveParameters;
                }                             
            };
        }
    }
}