﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;

namespace Video_Store_Web_Site.API
{
    public enum DataType { String, Integer, DateTime }

    public class ParameterValue
    {
        public DataType Type;
        public object Value;
    } 

   

    public class ParameterInfo
    {
        private Dictionary<string, ParameterValue> _parameters;

        public ParameterInfo()
        {
            _parameters = new Dictionary<string, ParameterValue>(8);

        }
        public ParameterValue this[string key]
        {
            get { return _parameters[key]; }
            set { _parameters[key] = value; }
        }

        public void SetParameters(IQuery query)
        {
            foreach (var name in _parameters.Keys)
            {
                var entry = _parameters[name];
                switch (entry.Type)
                {
                    case DataType.String:
                        query.SetString(name, (string) entry.Value);
                        break;

                    case DataType.Integer:
                        query.SetInt64(name, (int) entry.Value);
                        break;

                    case DataType.DateTime:
                        query.SetDateTime(name, (DateTime) entry.Value);
                        break;

                    default:
                        throw new ArgumentException("Data type " + entry.Type + " not handled");
                }
            }
        }
    }
}