﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using Nancy.ModelBinding;
using NHibernate;

namespace Video_Store_Web_Site.API
{

    class RentalResponse
    {
        public int Id { get; set; }
        public Video Video { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime RentalDate { get; set; }
        public DateTime? ReturnDate { get; set; }

        public RentalResponse(Model.Rental rental)
        {
            Id = rental.Id;
            Video = rental.Video;
            DueDate = rental.DueDate;
            RentalDate = rental.RentalDate;
            ReturnDate = rental.ReturnDate;
        }
    }

    public class Customer : BaseModule
    {
        public Customer() : base("customer")
        {            
            var dictionary = new Dictionary<int, Model.Customer>();
            dictionary[1] = Cusack;
            dictionary[2] = McFall;

            Get["/"] = _ =>
            {
                //  Replace this with code that
                //  1.  Gets the current session
                //  2.  Begins a transaction
                //  3.  Uses session.CreateQuery to create a query on the Customer object; no criteria are necessary
                //  4.  Uses the .List method on the Query object to get a list of all of the customers
                //  5.  Commits the transaction
                //  6.  Returns the list of customers that was found
                return dictionary.Values;
            };

            Get["/{id}"] = parameters =>
            {
                //  Replace this with code that
                //  1.  Gets the current session
                //  2.  Begins a transaction
                //  3.  Uses session.Load to load the Customer whose ID property matches the one that was passed, storing the loaded
                //      value into a variable named customer
                //  4.  Commits the transaction
                //  5.  Returns the customer object that was located in step 3
                int id = parameters.id;
                switch (id)
                {
                    case 1:
                        return Cusack;

                    case 2:
                        return McFall;

                    default:
                        return "Oops, customer ID passed for invalid customer";
                }                
            };

            Get["/{id}/rentals"] = parameters =>
            {
                var id = parameters.id;

                //  Replace this with code that 
                //  1.  Gets the current session
                //  2.  Begins a transaction 
                //  3.  Loads the customer with the ID value specified by the 'id' variable declared above
                //  4.  Creates a new list of RentalResponse objects based on the Rentals property of the loaded customer (see example below for one way to do it)
                //  5.  Commits the transaction
                //  6.  Returns the list of RentalResponse objects created in step 4 above.
                var rentals = id == 1 ? CusackRentals : McFallRentals;                
                return rentals.Select(r => new RentalResponse(r));
            };

            Post["/"] = _ =>
            {
                //  Replace this with code that
                //  1.  Gets the current session
                //  2.  Begins a transaction 
                //  3.  Uses session.Save to saves the customer.  
                //  4.  Commits the transaction
                //  5.  Returns the resulting customer object  
                var newCustomer = this.Bind<Model.Customer>();                
                return newCustomer;
            };

            Post["/{id}"] = parameters =>
            {
                

                //  the arguments to Bind are properties you don't want set.
                //  At this point the Password setter is failing, so we'll just ignore it
                var updated = this.Bind<Model.Customer>(c => c.Password);

                //  Add code that 
                //  1.  Gets the current session
                //  2.  Begins a transaction
                //  3.  Uses session.Load to load the customer whose ID is the value in parameters.id
                //  4.  Sets the properties of the loaded customer based on the values in the 'updated' object
                //  5.  Calls session.SaveOrUpdate on the loaded customer to persist the changes
                //  6.  Commits the transaction
                //  7.  Returns the updated customer object
                
                return updated;
            };
        }
    }
}