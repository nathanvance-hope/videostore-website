﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy.ModelBinding;

namespace Video_Store_Web_Site.API
{
    public class Genre : BaseModule
    {
        public Genre() : base("genre")
        {
            Get["/"] = _ =>
            {
                var session = _sessionFactory.GetCurrentSession();
                return session.QueryOver<Model.Genre>().List<Model.Genre>();
            };
        }
    }
}